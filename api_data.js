define({ "api": [
  {
    "type": "get",
    "url": "/auth/facebook/callback",
    "title": "Authenticate the callback after facebook has authenticated the user",
    "name": "Auth_facebook",
    "group": "Auth",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>facebook token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "User",
            "optional": false,
            "field": "user",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/auth/facebook/index.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/local",
    "title": "Authenticate user by email and password",
    "name": "Auth_local",
    "group": "Auth",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory account username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory account password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/auth/local/index.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/api/categories/:id",
    "title": "Gets a single Category from the DB",
    "name": "Get_Category",
    "group": "Category",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>language 'en' or 'th'. Example lang=th</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "Category",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/category/category.controller.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/api/categories/",
    "title": "Gets a list of Categorys",
    "name": "Get_Category_list",
    "group": "Category",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>language 'en' or 'th'. Example lang=th</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "Array",
            "description": "<p>of Category</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/category/category.controller.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/api/country-codes/:countryCodeId/city-codes",
    "title": "Get a list of City code based on country",
    "name": "Get_City_code",
    "group": "City_codes",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of City code</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/city-code/city-code.controller.js",
    "groupTitle": "City_codes"
  },
  {
    "type": "post",
    "url": "/api/coach-services/",
    "title": "Obsoleted Crate a new Coach topic",
    "name": "Create_a_new_Coach_Service",
    "group": "CoachService",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..60",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory Coach topic title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..200",
            "optional": false,
            "field": "description",
            "description": "<p>Mandatory Coach topic description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..200",
            "optional": false,
            "field": "benefit",
            "description": "<p>Mandatory Habit title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Mandatory Type support is '1-1 Coaching', 'Consultant', 'Training', 'Mentor Coaching'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>Mandatory Content language support 'en' or 'th'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serviceCategoryId",
            "description": "<p>Mandatory topic category</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "serviceCategorySpecializeIds",
            "description": "<p>Mandatory Array of Service Category SpecializeId</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "startingPrice",
            "description": "<p>Mandatory should equal to 0 or greater than 0.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "packages",
            "description": "<p>Optional coach package {name*, type*('Video call', 'Call', 'Offline', 'Text message'), freeTrial*(true, false), period(&gt;=1), duration(&gt;=1, interval('Daily', 'Once a week', 'Once a month', 'Custom')), price(&gt;=1), currency('THB')}</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": "<p>Optional</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "CoachService",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-service/coach-service.controller.js",
    "groupTitle": "CoachService"
  },
  {
    "type": "delete",
    "url": "/api/coach-services/:id",
    "title": "Obsoleted Delete coach topic",
    "name": "Delete_Coach_Service",
    "group": "CoachService",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Blank",
            "optional": false,
            "field": "No",
            "description": "<p>Content</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-service/coach-service.controller.js",
    "groupTitle": "CoachService"
  },
  {
    "type": "get",
    "url": "/api/coach-services/:topicId",
    "title": "Obsoleted Gets a single Coach topic by Id",
    "name": "Get_Coach_Service_detail",
    "group": "CoachService",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "CoachService",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-service/coach-service.controller.js",
    "groupTitle": "CoachService"
  },
  {
    "type": "get",
    "url": "/api/coach-services",
    "title": "Obsoleted Gets a list of Coach topics",
    "name": "Get_Coach_Service_list",
    "group": "CoachService",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Optional '-createdAt', default: '-createdAt', (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "langs[]",
            "description": "<p>Optional array of language 'en','th'. Example langs[]=th&amp;langs[]=en. Default is all lang.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>Optional filter by userId</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serviceCategoryId",
            "description": "<p>Optional id of category use for filtering</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "select",
            "description": "<p>Optional string for filter return fields. Example 'name,description'. Fields support: name,description,benefit,lang,photo,youtubeVideoLink,packages.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of Topics</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-service/coach-service.controller.js",
    "groupTitle": "CoachService"
  },
  {
    "type": "put",
    "url": "/api/coach-services/:id",
    "title": "Obsoleted Update Coach topic",
    "name": "Update_Coach_Service",
    "group": "CoachService",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..60",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory Coach topic title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..200",
            "optional": false,
            "field": "description",
            "description": "<p>Mandatory Coach topic description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..200",
            "optional": false,
            "field": "benefit",
            "description": "<p>Mandatory Habit title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>Mandatory Content language support 'en' or 'th'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Mandatory topic category</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "serviceCategorySpecializeIds",
            "description": "<p>Array of Service Category SpecializeId</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "youtubeVideoLink",
            "description": "<p>Optional youtube url</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "packages",
            "description": "<p>Optional coach package {name*, type*('Video call', 'Call', 'Offline', 'Text message'), freeTrial*(true, false), period(&gt;=1), duration(&gt;=1, interval('Daily', 'Once a week', 'Once a month', 'Custom')), price(&gt;=1), currency('THB')}</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": "<p>Optional</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "CoachService",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-service/coach-service.controller.js",
    "groupTitle": "CoachService"
  },
  {
    "type": "get",
    "url": "/api/users/:userId/coach-profiles",
    "title": "Get user with coach profile.",
    "name": "Get_Coach_Profile",
    "group": "Coach_Profile",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "User",
            "description": "<p>Coach profile with role as coach</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-profile/coach-profile.controller.js",
    "groupTitle": "Coach_Profile"
  },
  {
    "type": "get",
    "url": "/api/users/coach-profiles",
    "title": "Get list of coach profile.",
    "name": "Get_Coach_Profile_list",
    "group": "Coach_Profile",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Optional '-soldCount', '-overallRating', '-startingPrice', default: '-soldCount -overallRating', (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "sorts",
            "description": "<p>Optional '-soldCount', '-overallRating', '-startingPrice', default: '-soldCount -overallRating', (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId[]",
            "optional": false,
            "field": "serviceCategoryIds",
            "description": "<p>Filter coach that provide service category with array of service category id. Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId[]",
            "optional": false,
            "field": "serviceCategorySpecializeIds",
            "description": "<p>Filter coach that provide service category specialize with array of service category specialize id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "services",
            "description": "<p>Filter coach that provide service with array of service string ('1-1 Coaching', 'Consultant', 'Training', 'Mentor Coaching'). Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "minCoachingHours",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxCoachingHours",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "minPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "minRating",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxRating",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "minSoldCount",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxSoldCount",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "provideFreeTrial",
            "description": "<p>Filter coach that provide free trial package.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "licenses",
            "description": "<p>Filter coach that have specific licenses with array of license string. Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "languages",
            "description": "<p>Filter coach that can coach in many languages with array of languages string (en, th). Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "channels",
            "description": "<p>Filter coach with channel support ('faceToFace', 'phoneCall', 'skype', 'line', 'whatsApp', 'promptCoachApp').</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "cityCodeId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "cityCodeIds",
            "description": "<p>filter coach cities by list of city id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "freeTextSearch",
            "description": "<p>search coach profile from name, detail and service name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Paging",
            "description": "<p>array of Users Coach profile with role as coach</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-profile/coach-profile.controller.js",
    "groupTitle": "Coach_Profile"
  },
  {
    "type": "put",
    "url": "/api/users/coach-profiles/submit-coach-profile",
    "title": "Submit coach profile for review",
    "name": "Submit_Coach_Profile",
    "group": "Coach_Profile",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "User",
            "description": "<p>profile</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-profile/coach-profile.controller.js",
    "groupTitle": "Coach_Profile"
  },
  {
    "type": "put",
    "url": "/api/users/coach-profiles",
    "title": "Update user with coach profile.",
    "name": "Update_Coach_Profile",
    "group": "Coach_Profile",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Unique identify name. (limit 60)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "slogan",
            "description": "<p>(limit 1000)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "about",
            "description": "<p>(limit 8000)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "benefit",
            "description": "<p>(limit 8000)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country code object</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City code object</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "hobbies",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Datetime",
            "optional": false,
            "field": "birthDate",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "coachingHours",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "startingPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "licenses",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "languages",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "serviceTypes",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ObjectId[]",
            "optional": false,
            "field": "serviceCategoryRefs",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ObjectId[]",
            "optional": false,
            "field": "serviceCategorySpecializeRefs",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "provideFreeTrial",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "testimonial",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": "<p>Coach homepage url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "previewVideo",
            "description": "<p>Youtube link url</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "channels.faceToFace",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "channels.phoneCall",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "channels.skype",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "channels.line",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "channels.whatsApp",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "channels.promptCoachApp",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skypeAccount",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lineAccount",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "whatsAppAccount",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "WorkExperienceSchema[]",
            "optional": false,
            "field": "workExperiences",
            "description": "<p>Work experience object</p>"
          },
          {
            "group": "Parameter",
            "type": "EducationExperienceSchema[]",
            "optional": false,
            "field": "educationExperiences",
            "description": "<p>Education experience object</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "hadCorporateClients",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "clientsGender",
            "description": "<p>(noPreference: 0, femaleOnly: 1, femalePreferred: 2, malePreferred: 3)</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "ageOfClients.children",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "ageOfClients.teens",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "ageOfClients.youngAdults",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "ageOfClients.adults",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "ageOfClients.seniorsOrRetirees",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "User",
            "description": "<p>Coach profile with role as coach</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-profile/coach-profile.controller.js",
    "groupTitle": "Coach_Profile"
  },
  {
    "type": "put",
    "url": "/api/users/:userId/coach-verifies/",
    "title": "Update a Coach verify data",
    "name": "Update_CoachVerify",
    "group": "Coach_Verify",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>Mandatory only 'Male' or 'Female'</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "idCardPhoto",
            "description": "<p>Mandatory</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "CoachVerify",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/coach-verify/coach-verify.controller.js",
    "groupTitle": "Coach_Verify"
  },
  {
    "type": "get",
    "url": "/api/country-codes",
    "title": "Gets a list of Country code",
    "name": "Get_Country_code",
    "group": "Country_codes",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Optional '-createdAt', default: '-createdAt', (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10, maximum is 50 limit by mongodb</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of Country code</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/country-code/country-code.controller.js",
    "groupTitle": "Country_codes"
  },
  {
    "type": "...",
    "url": "ErrorCode",
    "title": "Display list of error code",
    "name": "ErrorCode",
    "group": "ErrorCode",
    "error": {
      "fields": {
        "Other": [
          {
            "group": "Other",
            "optional": false,
            "field": "OtherError",
            "description": "<p>600</p>"
          }
        ],
        "Query Param": [
          {
            "group": "Query Param",
            "optional": false,
            "field": "Invalid",
            "description": "<p>param 601</p>"
          }
        ],
        "Field CastError": [
          {
            "group": "Field CastError",
            "optional": false,
            "field": "ObjectID",
            "description": "<p>701</p>"
          },
          {
            "group": "Field CastError",
            "optional": false,
            "field": "Array",
            "description": "<p>702</p>"
          }
        ],
        "Field ValidatorError": [
          {
            "group": "Field ValidatorError",
            "optional": false,
            "field": "required",
            "description": "<p>801</p>"
          }
        ],
        "Authen": [
          {
            "group": "Authen",
            "optional": false,
            "field": "InvalidRefreshToken",
            "description": "<p>400</p>"
          },
          {
            "group": "Authen",
            "optional": false,
            "field": "TokenExpire",
            "description": "<p>401</p>"
          },
          {
            "group": "Authen",
            "optional": false,
            "field": "InvalidToken",
            "description": "<p>401</p>"
          }
        ],
        "Authorize": [
          {
            "group": "Authorize",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>403</p>"
          }
        ],
        "User": [
          {
            "group": "User",
            "optional": false,
            "field": "InvalidClientError",
            "description": "<p>1000</p>"
          },
          {
            "group": "User",
            "optional": false,
            "field": "DuplicateUserError",
            "description": "<p>1001</p>"
          },
          {
            "group": "User",
            "optional": false,
            "field": "EmailNotRegisteredError",
            "description": "<p>1002</p>"
          },
          {
            "group": "User",
            "optional": false,
            "field": "PasswordNotCorrectError",
            "description": "<p>1003</p>"
          },
          {
            "group": "User",
            "optional": false,
            "field": "MissingUserNameOrPasswordError",
            "description": "<p>1004</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n [\n   {\n     \"param\": \"email\",\n     \"code\": 1001\n   }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "server/config/express.js",
    "groupTitle": "ErrorCode"
  },
  {
    "type": "post",
    "url": "/api/fb-fanpage/photo/",
    "title": "Post photo to fb fanpage",
    "name": "Upload_fb_fanpage_photo",
    "group": "FBFanpage",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": "<p>Mandatory image file</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Optional</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Model",
            "optional": false,
            "field": "Habit",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/fb-fanpage/fb-fanpage.controller.js",
    "groupTitle": "FBFanpage"
  },
  {
    "type": "post",
    "url": "/api/feeds/",
    "title": "Crate a new Feed",
    "name": "Create_a_new_Feed",
    "group": "Feeds",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..200",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory feed title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Mandatory feed description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Mandatory feed type should be 'Article' or 'Quote'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Optional feed category</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": "<p>Optional</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Feed",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/feed/feed.controller.js",
    "groupTitle": "Feeds"
  },
  {
    "type": "delete",
    "url": "/api/feeds/:id",
    "title": "Deletes a Feed",
    "name": "Delete_Feed",
    "group": "Feeds",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Blank",
            "optional": false,
            "field": "String",
            "description": "<p>empty response</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/feed/feed.controller.js",
    "groupTitle": "Feeds"
  },
  {
    "type": "get",
    "url": "/api/feeds/:id",
    "title": "Gets a single Feed by Id",
    "name": "Get_Feed_detail",
    "group": "Feeds",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Feed",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/feed/feed.controller.js",
    "groupTitle": "Feeds"
  },
  {
    "type": "get",
    "url": "/api/feeds",
    "title": "Gets a list of Feed",
    "name": "Get_Feed_list",
    "group": "Feeds",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Optional '-createdAt', default: '-createdAt', '-publishedAt' (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "status[]",
            "description": "<p>Optional array of filter by my-habit status 'Draft' and 'Publish'</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>Optional filter by userId</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Optional id of category use for filtering</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of Topics</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/feed/feed.controller.js",
    "groupTitle": "Feeds"
  },
  {
    "type": "put",
    "url": "/api/feeds/:id",
    "title": "Update feed by id",
    "name": "Update_a_new_Feed",
    "group": "Feeds",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..200",
            "optional": false,
            "field": "name",
            "description": "<p>Optional Coach topic title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Optional Coach topic description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Optional topic category</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": "<p>Optional</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Feed",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/feed/feed.controller.js",
    "groupTitle": "Feeds"
  },
  {
    "type": "post",
    "url": "/api/habits",
    "title": "Creates a new Habit in the DB",
    "name": "Create_Habit",
    "group": "Habit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": "<p>Basic info</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..60",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory Habit title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "0..500",
            "optional": false,
            "field": "benefit",
            "description": "<p>Optional Habit benefit description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"en\"",
              "\"th\""
            ],
            "optional": false,
            "field": "lang",
            "defaultValue": "en",
            "description": "<p>Optional language for habit.</p> <p>Status</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"Draft\"",
              "\"Completed\""
            ],
            "optional": false,
            "field": "status",
            "defaultValue": "Draft",
            "description": "<p>Optional habit status.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isPublic",
            "defaultValue": "true",
            "description": "<p>Optional true mean allow everyone to join, false is private habit.</p> <p>Price</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0..",
            "optional": false,
            "field": "price",
            "defaultValue": "0",
            "description": "<p>Optional 0 mean free</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"USD\"",
              "\"THB\""
            ],
            "optional": false,
            "field": "currency",
            "defaultValue": "USD",
            "description": "<p>Optional</p> <p>Course</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isCourse",
            "defaultValue": "false",
            "description": "<p>Optional if true mean it is Course with include tasks.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0..",
            "optional": false,
            "field": "totalDays",
            "defaultValue": "0",
            "description": "<p>Optional Total days if habit is course. (must greater than one if type is course)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0..",
            "optional": false,
            "field": "totalTasks",
            "defaultValue": "0",
            "description": "<p>Optional Total number of task (must equal number of task).</p>"
          },
          {
            "group": "Parameter",
            "type": "Task[]",
            "optional": false,
            "field": "tasks",
            "defaultValue": "[]",
            "description": "<p>Optional If isCourse is true and total task should equal to totalDays</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isAllowCustomDate",
            "defaultValue": "false",
            "description": "<p>Optional</p> <p>Ref</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Mandatory Habit category</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Task-Example:",
          "content": "{\n  \"name\": \"Task name with min 0 and max 200 characters.\",\n  \"description\": \"task description with max 500 characters.\",\n  \"farFormPreviousDays\": 1  // With default 1 day, mean distance between previous task\n}",
          "type": "json"
        },
        {
          "title": "Habit-Example:",
          "content": "{\n   \"name\": \"วิ่งสู้ฟัด\",\n   \"benefit\": \"ลดน้ำหนัก\",\n   \"lang\": \"th\",\n   \"userRef\": {\n     \"lastname\": \"Sanurt\",\n     \"firstname\": \"Atthaboon\",\n     \"_id\": \"58081f35046e868c1fe43156\"\n   },\n   \"categoryId\": \"312000000000000000000004\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "Habit",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit/habit.controller.js",
    "groupTitle": "Habit"
  },
  {
    "type": "delete",
    "url": "/api/habits/:habitId",
    "title": "Deletes a Habit by set status tobe 'deleted'",
    "name": "Delete_Habit",
    "group": "Habit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Blank",
            "optional": false,
            "field": "String",
            "description": "<p>empty response</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit/habit.controller.js",
    "groupTitle": "Habit"
  },
  {
    "type": "get",
    "url": "/api/habits/:habitId",
    "title": "Gets a single Habit by Id",
    "name": "Get_Habit_detail",
    "group": "Habit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Habits",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit/habit.controller.js",
    "groupTitle": "Habit"
  },
  {
    "type": "get",
    "url": "/api/habits",
    "title": "Gets a list of Habits",
    "name": "Get_Habit_list",
    "group": "Habit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Optional '-createdAt', default: '-createdAt', (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Optional 'course' or 'habit', default: undefined mean get all</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>Optional search by name of habit</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "langs[]",
            "description": "<p>Optional array of language 'en','th'. Example langs[]=th&amp;langs[]=en. Default is all lang.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Optional id of category use for filtering</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>Optional filter by userId</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "status[]",
            "description": "<p>Optional array of filter by habit status 'Draft', 'Completed' and 'Deleted'. Default is all status.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isPublic",
            "description": "<p>Optional filter by public or not</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "includeMyPrivate",
            "description": "<p>Optional special option included authen user private habit into a search list with the same condition the parsing param</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of Habits Question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit/habit.controller.js",
    "groupTitle": "Habit"
  },
  {
    "type": "put",
    "url": "/api/habits/:habitId",
    "title": "Updates an existing Habit in the DB",
    "name": "Update_Habit",
    "group": "Habit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": "<p>Basic info</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "1..60",
            "optional": false,
            "field": "name",
            "description": "<p>Optional Habit title name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "0..500",
            "optional": false,
            "field": "benefit",
            "description": "<p>Optional Habit benefit description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"en\"",
              "\"th\""
            ],
            "optional": false,
            "field": "lang",
            "defaultValue": "en",
            "description": "<p>Optional language for habit.</p> <p>Status</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"Draft\"",
              "\"Completed\""
            ],
            "optional": false,
            "field": "status",
            "description": "<p>Optional if completed not allow to update any value except change isPublic tobe 'true'</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isPublic",
            "defaultValue": "true",
            "description": "<p>Optional is allow everyone to join.</p> <p>Price</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0..",
            "optional": false,
            "field": "price",
            "defaultValue": "0",
            "description": "<p>Optional</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"USD\"",
              "\"THB\""
            ],
            "optional": false,
            "field": "currency",
            "defaultValue": "USD",
            "description": "<p>Optional</p> <p>Course</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isCourse",
            "defaultValue": "false",
            "description": "<p>Optional if true mean it is Course with include tasks.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0..",
            "optional": false,
            "field": "totalDays",
            "defaultValue": "0",
            "description": "<p>Optional Total days if habit is course. (must greater than one if type is course)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0..",
            "optional": false,
            "field": "totalTasks",
            "defaultValue": "0",
            "description": "<p>Optional Total number of task (must equal number of task).</p>"
          },
          {
            "group": "Parameter",
            "type": "Task[]",
            "optional": false,
            "field": "tasks",
            "defaultValue": "[]",
            "description": "<p>Optional isCourse have value true and total task should equal to totalDays</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isAllowCustomDate",
            "defaultValue": "false",
            "description": "<p>Optional allow to custom course date when join</p> <p>Ref</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Mandatory Habit category</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit/habit.controller.js",
    "groupTitle": "Habit"
  },
  {
    "type": "post",
    "url": "/api/habits/:habitId/photo/",
    "title": "Upload habit photo",
    "name": "Upload_Habit_photo",
    "group": "Habit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "Habit",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit/habit.controller.js",
    "groupTitle": "Habit"
  },
  {
    "type": "post",
    "url": "/api/habit-questions/:habitQuestionId/habit-answers",
    "title": "Creates a new HabitAnswer in the DB",
    "name": "Create_Habit_Answer",
    "group": "Habit_Answer",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mandatory</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "HabitAnswer",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-answer/habit-answer.controller.js",
    "groupTitle": "Habit_Answer"
  },
  {
    "type": "get",
    "url": "/api/habit-questions/:habitQuestionId/habit-answers/:id",
    "title": "Gets a single HabitAnswer from the DB",
    "name": "Get_Habit_Answer",
    "group": "Habit_Answer",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "HabitAnswer",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-answer/habit-answer.controller.js",
    "groupTitle": "Habit_Answer"
  },
  {
    "type": "get",
    "url": "/api/habit-questions/:habitQuestionId/habit-answers",
    "title": "Gets a list of HabitAnswers",
    "name": "Get_Habit_Answer_list",
    "group": "Habit_Answer",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>'-createdAt test', default: '-createdAt', desc is minus sign</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>default: 10</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of Habit Answer</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-answer/habit-answer.controller.js",
    "groupTitle": "Habit_Answer"
  },
  {
    "type": "post",
    "url": "/api/habits/:habitId/habit-questions/",
    "title": "Creates a new HabitQuestion in the DB",
    "name": "Create_Habit_Question",
    "group": "Habit_Question",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Mandatory</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "Habit",
            "description": "<p>Question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-question/habit-question.controller.js",
    "groupTitle": "Habit_Question"
  },
  {
    "type": "get",
    "url": "/api/habits/:habitId/habit-questions/:id",
    "title": "Gets a single HabitQuestion from the DB",
    "name": "Get_Habit_Question",
    "group": "Habit_Question",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "Habit",
            "description": "<p>Question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-question/habit-question.controller.js",
    "groupTitle": "Habit_Question"
  },
  {
    "type": "get",
    "url": "/api/habits/:habitId/habit-questions",
    "title": "Gets a list of HabitQuestions based on habit",
    "name": "Get_Habit_Question_list",
    "group": "Habit_Question",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "habitId",
            "description": "<p>filter by habitId</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>'-createdAt test', default: '-createdAt', desc is minus sign</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>default: 10</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of Habits Question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-question/habit-question.controller.js",
    "groupTitle": "Habit_Question"
  },
  {
    "type": "post",
    "url": "/api/habit-answers/:habitAnswerId/habit-replies/",
    "title": "Creates a new HabitReply in the DB",
    "name": "Create_Habit_Reply",
    "group": "Habit_Reply",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mandatory</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "HabitReply",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-reply/habit-reply.controller.js",
    "groupTitle": "Habit_Reply"
  },
  {
    "type": "get",
    "url": "/api/habit-answers/:habitAnswerId/habit-replies/:id",
    "title": "Gets a single HabitReply from the DB",
    "name": "Get_Habit_Reply",
    "group": "Habit_Reply",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "HabitReply",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-reply/habit-reply.controller.js",
    "groupTitle": "Habit_Reply"
  },
  {
    "type": "get",
    "url": "/api/habit-answers/:habitAnswerId/habit-replies",
    "title": "Gets a list of HabitReply",
    "name": "Get_Habit_Reply_list",
    "group": "Habit_Reply",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>'-createdAt test', default: '-createdAt', desc is minus sign</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>default: 10</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of Habits Reply</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/habit-reply/habit-reply.controller.js",
    "groupTitle": "Habit_Reply"
  },
  {
    "type": "post",
    "url": "/api/inbox-messages/",
    "title": "Create a new inbox message",
    "name": "Create_a_new_Inbox_message",
    "group": "InboxMessage",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory Sender name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory Sender email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Optional Sender phone number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Mandatory support (0 is request-consultation)</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "receiverId",
            "description": "<p>Mandatory user id of receiver</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "InboxMessage",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/inbox-message/inbox-message.controller.js",
    "groupTitle": "InboxMessage"
  },
  {
    "type": "get",
    "url": "/api/inbox-messages/:id",
    "title": "Gets a single messages from the DB",
    "name": "Get_Inbox_message",
    "group": "InboxMessage",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "InboxMessage",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/inbox-message/inbox-message.controller.js",
    "groupTitle": "InboxMessage"
  },
  {
    "type": "get",
    "url": "/api/inbox-messages",
    "title": "Get list of inbox messages that belong to login user",
    "name": "Get_inbox_message_list",
    "group": "InboxMessage",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Mandatory type support (0 is request-consultation)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Paging",
            "description": "<p>array of InboxMessage</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/inbox-message/inbox-message.controller.js",
    "groupTitle": "InboxMessage"
  },
  {
    "type": "post",
    "url": "/api/my-habits",
    "title": "Creates a new MyHabit in the DB",
    "name": "Create_MyHabit",
    "group": "MyHabits",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "startDate",
            "description": "<p>Mandatory My Habit start date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "endDate",
            "description": "<p>Optional My Habit end date (require if habit type is 'Habit').</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "repeatType",
            "description": "<p>Optional 'weekly' or 'monthly' (require if habit type is 'Habit').</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean[]",
            "optional": false,
            "field": "daysOfWeek",
            "description": "<p>Optional (require if repeatType type is 'weekly').</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "dayOfMonth",
            "description": "<p>Optional (require for repeatType is monthly) day of month start from 1 to 31</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean[]",
            "optional": false,
            "field": "monthsOfYear",
            "description": "<p>Optional (require for repeatType is monthly) is array of select which month of year(start form January - December) with boolean value (true|false). Example dayOfMonth=5&amp;daysOfWeek[0]=true&amp;daysOfWeek[1]=false&amp;daysOfWeek[2]=false&amp;daysOfWeek[3]=false&amp;daysOfWeek[4]=false&amp;daysOfWeek[5]=false&amp;daysOfWeek[6]=true&amp;daysOfWeek[7]=true&amp;daysOfWeek[8]=true&amp;daysOfWeek[9]=true&amp;daysOfWeek[10]=true&amp;daysOfWeek[11]=true</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "private",
            "description": "<p>Mandatory allow to public statistic</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "myGoal",
            "description": "<p>Mandatory display goal that user set</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "happenIfDo",
            "description": "<p>Mandatory display user reason</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "happenIfNotDo",
            "description": "<p>Mandatory display user reason</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notHappenIfDo",
            "description": "<p>Mandatory display user reason</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notHappenIfNotDo",
            "description": "<p>Mandatory display user reason</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "reminder",
            "description": "<p>Mandatory should remind user or not</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "reminderHour",
            "description": "<p>Optional (require if reminder is true).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reminderMinutes",
            "description": "<p>Optional (require if reminder is true).</p> <p>Reference</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "habitId",
            "description": "<p>Mandatory</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "MyHabit-Join Habit Example:",
          "content": "{\n   \"startDate\": \"2016-11-01\",\n   \"endDate\": \"2016-11-15\",\n   \"daysOfWeek\": [true,true,true,true,true,true,true],\n   \"habitId\": \"811000000000000000000004\",\n   \"userId\": \"58081f35046e868c1fe43156\",\n   \"reminder\": false,\n   \"myGoal\": \"หุ่นดี\",\n   \"private\": false\n}",
          "type": "json"
        },
        {
          "title": "MyHabit-Join Course Example:",
          "content": "{\n   \"startDate\": \"2016-11-01\",\n   \"habitId\": \"811000000000000000000004\",\n   \"userId\": \"58081f35046e868c1fe43156\",\n   \"reminder\": false,\n   \"myGoal\": \"หุ่นดี\",\n   \"private\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "MyHabit",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-habit/my-habit.controller.js",
    "groupTitle": "MyHabits"
  },
  {
    "type": "delete",
    "url": "/api/my-habits/:id",
    "title": "Change MyHabit status tobe 'Deleted'",
    "name": "Delete_MyHabit",
    "group": "MyHabits",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Blank",
            "optional": false,
            "field": "No",
            "description": "<p>Content</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-habit/my-habit.controller.js",
    "groupTitle": "MyHabits"
  },
  {
    "type": "get",
    "url": "/api/my-habits/:id",
    "title": "Gets a single MyHabit from the DB",
    "name": "Get_MyHabit",
    "group": "MyHabits",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "populate[]",
            "description": "<p>Optional support Habit model. populate[]=habit</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "habitFields",
            "description": "<p>Optional select habit field to populate. default is '_id,name,photo'</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Model",
            "optional": false,
            "field": "MyHabit",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-habit/my-habit.controller.js",
    "groupTitle": "MyHabits"
  },
  {
    "type": "get",
    "url": "/api/my-habits",
    "title": "Gets a list of MyHabits",
    "name": "Get_MyHabit_list",
    "group": "MyHabits",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Optional '-createdAt', default: '-createdAt', (asc no sign, desc put minus sign)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Optional 'course' or 'habit', default: undefined mean get all</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "status[]",
            "description": "<p>Optional array of filter by my-habit status 'Completed' and 'Deleted'</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "populate[]",
            "description": "<p>Optional support Habit model. populate[]=habit</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "habitFields",
            "description": "<p>Optional select habit field to populate. default is '_id,name,photo'</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of MyHabit</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-habit/my-habit.controller.js",
    "groupTitle": "MyHabits"
  },
  {
    "type": "put",
    "url": "/api/my-habits/:id",
    "title": "Updates an existing MyHabit in the DB",
    "name": "Update_MyHabit",
    "group": "MyHabits",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "reminder",
            "description": "<p>Mandatory should remind user or not</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "reminderHour",
            "description": "<p>Optional (require if reminder is true).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reminderMinutes",
            "description": "<p>Optional (require if reminder is true).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "MyHabit-Join Habit Example:",
          "content": "{\n\t  \"reminder\": false,\n\t  \"reminderHour\": 2,\n\t  \"reminderMinutes\": '00'\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Model",
            "optional": false,
            "field": "MyHabit",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-habit/my-habit.controller.js",
    "groupTitle": "MyHabits"
  },
  {
    "type": "post",
    "url": "/api/my-tasks/",
    "title": "Create a new MyTask in the DB",
    "name": "Complete_My_task__Create_myTask__Cancel_push_notification_and_Delete_notification_entity",
    "group": "MyTasks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "populate[]",
            "description": "<p>Optional [query] string support MyHabit model. populate[]=myHabit</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "myHabitId",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "taskDate",
            "description": "<p>Mandatory task date - allow only current date</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-task/my-task.controller.js",
    "groupTitle": "MyTasks"
  },
  {
    "type": "delete",
    "url": "/api/my-tasks/:id",
    "title": "Delete my complete task",
    "name": "Delete_MyTask__Add_push_notification_if_not_reach_task_time_and_add_notification_entity",
    "group": "MyTasks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "json",
            "optional": false,
            "field": "Success-Response:",
            "description": "<p>HTTP/1.1 200 OK { &quot;_id&quot;: null, &quot;name&quot;: &quot;habit name&quot;, &quot;myHabitId&quot;: &quot;581d9142b70220401b7a4a09&quot;, &quot;taskDate&quot;: &quot;2016-10-31T17:00:00.000Z&quot;, &quot;completeDate&quot;: null, &quot;isDelay&quot;: false, &quot;reminder&quot;: true, &quot;reminderHour&quot;: 2, &quot;reminderMinutes&quot;: &quot;10&quot; }</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/my-task/my-task.controller.js",
    "groupTitle": "MyTasks"
  },
  {
    "type": "get",
    "url": "/api/my-tasks/course-preview/:id",
    "title": "Get preview of course calendar",
    "name": "Get_Course_calendar_preview",
    "group": "MyTasks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Mandatory query habitId type course that want to preview.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "startDate",
            "description": "<p>Mandatory first day of preview task date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "endDate",
            "description": "<p>Mandatory last day of preview task date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "habitStartDate",
            "description": "<p>Mandatory habit start date. format yyyy-mm-dd</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "Calendar",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  [\n    {\n      \"_id\": null or '111111111111111111'\n      \"myHabitId\": null,\n      \"taskDate\": \"2016-10-31T17:00:00.000Z\",\n      \"completeDate\": null,\n      \"isDelay\": false\n    }\n    ...\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "server/api/my-task/my-task.controller.js",
    "groupTitle": "MyTasks"
  },
  {
    "type": "get",
    "url": "/api/my-tasks/habit-preview",
    "title": "Get preview of Habit Calendar",
    "name": "Get_Habit_calendar_preview",
    "group": "MyTasks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "startDate",
            "description": "<p>Mandatory first day of preview task date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "endDate",
            "description": "<p>Mandatory last day of preview task date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "habitStartDate",
            "description": "<p>Mandatory habit start date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "habitEndDate",
            "description": "<p>Mandatory habit end date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "repeatType",
            "description": "<p>Mandatory 'weekly' or 'monthly'</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean[]",
            "optional": false,
            "field": "daysOfWeek",
            "description": "<p>Optional (require for repeatType is weekly) is array of select which day of week(start form sunday - saturday) with boolean value (true|false). Example daysOfWeek[]=true,daysOfWeek=false,daysOfWeek[]=true,daysOfWeek[]=false,daysOfWeek[]=false,daysOfWeek[]=false,daysOfWeek[]=false</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "dayOfMonth",
            "description": "<p>Optional (require for repeatType is monthly) day of month start from 1 to 31</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean[]",
            "optional": false,
            "field": "monthsOfYear",
            "description": "<p>Optional (require for repeatType is monthly) is array of select which month of year(start form January - December) with boolean value (true|false). Example dayOfMonth=5&amp;daysOfWeek[0]=true&amp;daysOfWeek[1]=false&amp;daysOfWeek[2]=false&amp;daysOfWeek[3]=false&amp;daysOfWeek[4]=false&amp;daysOfWeek[5]=false&amp;daysOfWeek[6]=true&amp;daysOfWeek[7]=true&amp;daysOfWeek[8]=true&amp;daysOfWeek[9]=true&amp;daysOfWeek[10]=true&amp;daysOfWeek[11]=true</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "Calendar",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  [\n    {\n      \"myHabitId\": null,\n      \"taskDate\": \"2016-10-31T17:00:00.000Z\",\n      \"completeDate\": null,\n      \"isDelay\": false\n    }\n    ...\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/my-tasks/habit-preview?startDate=2016-10-01&endDate=2017-12-30&daysOfWeek[0]=true&daysOfWeek[1]=false&daysOfWeek[2]=false&daysOfWeek[3]=false&daysOfWeek[4]=false&daysOfWeek[5]=false&daysOfWeek[6]=true\ncurl -i http://localhost:3000/api/my-tasks/habit-preview?startDate=2016-11-01&endDate=2016-11-30&habitStartDate=2016-10-01&habitEndDate=2016-12-30&repeatType=monthly&dayOfMonth=2&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true&monthsOfYear[]=true",
        "type": "curl"
      }
    ],
    "version": "0.0.0",
    "filename": "server/api/my-task/my-task.controller.js",
    "groupTitle": "MyTasks"
  },
  {
    "type": "get",
    "url": "/api/my-tasks/",
    "title": "Gets a list of MyTasks based on myHabit and date range",
    "name": "Get_MyTasks_list",
    "group": "MyTasks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "startDate",
            "description": "<p>Mandatory first day of preview task date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "endDate",
            "description": "<p>Mandatory last day of preview task date. format yyyy-mm-dd</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "myHabitId",
            "description": "<p>Optional filter by myHabitId</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "myHabitStatus[]",
            "description": "<p>Optional array of filter by habit status 'Inprogress', 'Completed' and 'Deleted'. Default is all status.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of MyTask</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  [\n    {\n      \"_id\": null,\n      \"name\": \"habit name\",\n      \"myHabitId\": \"581d9142b70220401b7a4a09\",\n      \"taskDate\": \"2016-10-31T17:00:00.000Z\",\n      \"completeDate\": null,\n      \"isDelay\": false,\n      \"reminder\": true,\n      \"reminderHour\": 2,\n      \"reminderMinutes\": \"10\"\n    }\n    ...\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "server/api/my-task/my-task.controller.js",
    "groupTitle": "MyTasks"
  },
  {
    "type": "get",
    "url": "/api/my-tasks/delay-tasks",
    "title": "Gets a list of My delay tasks",
    "name": "Get_My_delay_task_list",
    "group": "MyTasks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of MyTask</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  [\n    {\n      \"_id\": null,\n      \"name\": \"habit name\",\n      \"myHabitId\": \"581d9142b70220401b7a4a09\",\n      \"taskDate\": \"2016-10-31T17:00:00.000Z\",\n      \"completeDate\": null,\n      \"isDelay\": false,\n      \"reminder\": true,\n      \"reminderHour\": 2,\n      \"reminderMinutes\": \"10\"\n    }\n    ...\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "server/api/my-task/my-task.controller.js",
    "groupTitle": "MyTasks"
  },
  {
    "type": "get",
    "url": "/api/notifications/",
    "title": "Gets a list of notifications based on current time",
    "name": "Get_notification_list",
    "group": "Notifications",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of Notification</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/notification/notification.controller.js",
    "groupTitle": "Notifications"
  },
  {
    "type": "put",
    "url": "/api/notifications/:id",
    "title": "Update Notification to be view",
    "name": "update_notification",
    "group": "Notifications",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Model",
            "optional": false,
            "field": "Notification",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/notification/notification.controller.js",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "/api/service-categories",
    "title": "Get a list of service categories",
    "name": "Get_ServiceCategory",
    "group": "ServiceCategory",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>language 'en' or 'th'. Example lang=th</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of Service category</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/service-category/service-category.controller.js",
    "groupTitle": "ServiceCategory"
  },
  {
    "type": "get",
    "url": "/api/service-category-specializes",
    "title": "Get a list of service category specializes",
    "name": "Get_ServiceCategorySpecialize",
    "group": "ServiceCategorySpecialize",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>language 'en' or 'th'. Example lang=th</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serviceCategoryId",
            "description": "<p>Optional id of service category use for filtering</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Array",
            "description": "<p>of Service category specialize</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/service-category-specialize/service-category-specialize.controller.js",
    "groupTitle": "ServiceCategorySpecialize"
  },
  {
    "type": "post",
    "url": "/api/users",
    "title": "Create a new User",
    "name": "Create_User",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/x-www-form-urlencoded",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email as a username. Must be unique.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password at least 6 characters</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>(Optional)</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "avatar",
            "description": "<p>(Optional)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "oAuth2",
            "optional": false,
            "field": "oAuth2Token",
            "description": ""
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example response",
        "content": "{\n  token_type: \"bearer\",\n  access_token: \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ...\",\n  expires_in: 86400,\n  refresh_token: \"9a37a1db568c1e11fd1b2ecddfccc...\"\n}",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users/coach-profiles/:id",
    "title": "Get coach profile by id",
    "name": "Get_Coach_Profile",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "User",
            "description": "<p>with role as coach</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users/coach-profiles",
    "title": "Get list of coach profile.",
    "name": "Get_Coach_Profile_list",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId[]",
            "optional": false,
            "field": "serviceCategoryIds",
            "description": "<p>Filter coach that provide service category with array of service category id. Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "services",
            "description": "<p>Filter coach that provide service with array of service string ('1-1 Coaching', 'Consultant', 'Training', 'Mentor Coaching'). Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "minPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "provideFreeTrial",
            "description": "<p>Filter coach that provide free trial package.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "minRating",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxRating",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "licenses",
            "description": "<p>Filter coach that have specific licenses with array of license string. Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "languages",
            "description": "<p>Filter coach that can coach in many languages with array of languages string (en, th). Note filter by or condition</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "channels",
            "description": "<p>Filter coach with channel support ('Skype', 'Line', 'Face to face').</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "cityCodeId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "freeTextSearch",
            "description": "<p>search coach profile from name, detail and service name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Paging",
            "description": "<p>array of Users with role as coach</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users",
    "title": "Gets a list of Users. restriction: 'admin'",
    "name": "Get_User_list",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Optional default: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Optional default: 10</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Models",
            "optional": false,
            "field": "Paging",
            "description": "<p>array of Users</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users/me",
    "title": "Get my info",
    "name": "Me",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "User",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/users/",
    "title": "Update user profile",
    "name": "Update_User",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "WheelOfLife[]",
            "optional": false,
            "field": "Optional",
            "description": "<p>wheel of life data ([{categoryId: 0001, value: 1}....10])</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "User",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/users/avatar/",
    "title": "Upload user avatar",
    "name": "Upload_user_avatar",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "multipart/form-data",
            "description": "<p>;</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "avatar",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "User",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/users/push-notification-devices/:id",
    "title": "Add push notification device subscribe for login user",
    "name": "add_push_notification_devices",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "User",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/api/users/push-notification-devices/:id",
    "title": "Delete push notification device from subscribe user",
    "name": "delete_push_notification_devices",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "deviceId",
            "description": "<p>Mandatory push notification deviceId</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Model",
            "optional": false,
            "field": "User",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/user/user.controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/version",
    "title": "Get current api version",
    "name": "get_api_version",
    "group": "Version",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/json",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "api",
            "description": "<p>version format major.minor.patch</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/api/version/version.controller.js",
    "groupTitle": "Version"
  },
  {
    "type": "post",
    "url": "/auth/facebook-token/token",
    "title": "Authenticate with facebook token",
    "name": "Auth_facebook_Token",
    "group": "oAuth2",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/x-www-form-urlencoded",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Authorization",
            "optional": false,
            "field": "Bearer",
            "description": "<p>fbtoken</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "client_id",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Mandatory</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "oAuth2",
            "optional": false,
            "field": "oAuth2Token",
            "description": ""
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example response",
        "content": "{\n  token_type: \"bearer\",\n  access_token: \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ...\",\n  expires_in: 86400,\n  refresh_token: \"9a37a1db568c1e11fd1b2ecddfccc...\"\n}",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "server/auth/facebook-token/index.js",
    "groupTitle": "oAuth2"
  },
  {
    "type": "post",
    "url": "/oauth/token",
    "title": "Get oAuth2 token",
    "name": "Get_Token",
    "group": "oAuth2",
    "description": "<p>Remark access token will expire in 24 hours. Get new token by call /oauth/token with grant_type 'refresh_token'.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "application/x-www-form-urlencoded",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "grant_type",
            "description": "<p>Mandatory option is 'password', 'refresh_token'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "client_id",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Mandatory</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Optional require only grant_type 'refresh_token'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>Optional require only grant_type 'password'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Optional require only grant_type 'password'</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "oAuth2",
            "optional": false,
            "field": "oAuth2Token",
            "description": ""
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example request grant_type='password'",
        "content": "{\n grant_type:     password,\n client_id:      clientId,\n client_secret:  clientSecret,\n username:       email,\n password:       password\n}",
        "type": "json"
      },
      {
        "title": "Example request grant_type='refresh_token'",
        "content": "{\n grant_type:     refresh_token,\n client_id:      clientId,\n client_secret:  clientSecret,\n refresh_token:  token\n}",
        "type": "json"
      },
      {
        "title": "Example response",
        "content": "{\n  token_type: \"bearer\",\n  access_token: \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ...\",\n  expires_in: 86400,\n  refresh_token: \"9a37a1db568c1e11fd1b2ecddfccc...\"\n}",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "server/auth/oauth2/index.js",
    "groupTitle": "oAuth2"
  }
] });
